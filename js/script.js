document.addEventListener('DOMContentLoaded', function(){

var newsFeed = document.getElementById("newsFeed");
newsFeed.classList.add("_posts");


var postAuthor = document.getElementById("postAuthor");
var postText = document.getElementById("postText");
var imageUrl = document.getElementById("imageUrl");
var inputFile = document.getElementById("inputFile");
var btnPost = document.getElementById("btnPost");

//Функция-конструктор для поста
function Post(person, text, image) {
  this.author = person;
  this.date = new Date();
  this.text = text;
  this.image = image;
}
